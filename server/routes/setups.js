var express = require('express');
var router = express.Router();
var base = require('../config/Database'); 

router.get('/', function(req, res, next) {
	base.fetch('/setups', {
		context: this, 
		asArray: true, 
		then(data){
			res.json(data)
		}
	})
});

module.exports = router;
