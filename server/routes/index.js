var express = require('express');
var router = express.Router();
var base = require('../config/Database'); 


const universalLoader = require('../universal')

router.get('/', universalLoader)

module.exports = router;
